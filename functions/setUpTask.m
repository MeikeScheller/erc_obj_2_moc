function [params,data] = setUpTask(params,part)

% mean audio frequency (mapped to background, or depth of screen)
params.meanFreq = 600;

% Samples per second. Should at least double the max frequency. 
% 44.1kHz is also okay.  
params.Fs = 48000;

% reference weight (in grams)
params.refWeight = 500;

% half weight range (in grams)
params.halfrange = 350;

% how many unique stimuli for method of constants?
params.nUnique = 6;

% get the unique stimuli
params.comparisons = linspace(...
    params.refWeight-params.halfrange,...
    params.refWeight+params.halfrange,...
    params.nUnique);

% how many times to repeat each stimulus?
params.nReps = 3; % 10? 

% how often to take a break? (after how many trials?)
params.breakInt = 10;

% set up the trial structure
% trial_number | reference_weight | comparison_weight | reference_first? |
% chosen_as_heavier | correct?
data.trials = nan(length(params.comparisons)*params.nReps,6);
data.trials(:,1) = 1:length(params.comparisons)*params.nReps;
data.trials(:,2) = params.refWeight;
data.trials(:,3) = repmat(params.comparisons',[params.nReps,1]);
data.trials(:,4) = randi(2,[length(params.comparisons)*params.nReps,1])-1;
data.trials(:,2:end) = data.trials(randperm(size(data.trials,1)),2:end);

% add the participant info to the data structure
data.part = part;

end










